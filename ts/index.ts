import * as plugins from './smarthbs.plugins.js';
export type TTemplateStringType = 'filePath' | 'code';

export let handlebars = plugins.handlebars;
export * from './smarthbs.compile.js';
import './smarthbs.helpers.js';
export * from './smarthbs.partials.js';
export * from './smarthbs.template.js';
export * from './smarthbs.variables.js';
export * from './smarthbs.postprocess.js';
