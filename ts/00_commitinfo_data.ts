/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@pushrocks/smarthbs',
  version: '3.0.3',
  description: 'handlebars with better fs support'
}
