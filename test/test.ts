import { expect, tap } from '@pushrocks/tapbundle';
import * as smarthbs from '../ts/index.js';

import * as smartpath from '@pushrocks/smartpath';
import * as path from 'path';

const dirname = smartpath.get.dirnameFromImportMetaUrl(import.meta.url);

let hbs_testfilesDir = path.join(dirname, 'hbs_testfiles');
let testPartialDir = path.join(hbs_testfilesDir, 'partials');
let testResultDir = path.join(dirname, 'testresult');

tap.test('smarthbs -> should create partials', async () => {
  await smarthbs.registerPartialDir(testPartialDir);
});

tap.test('smarthbs -> should compile a directory', async () => {
  smarthbs.compileDirectory(hbs_testfilesDir, testResultDir, 'data.json');
});

tap.test('', async () => {
  let templateString = '{{{firstVar}}} {{secondVar}}';
  let templateVars = await smarthbs.findVarsInHbsString(templateString);
  expect(templateVars).toContain('firstVar');
  expect(templateVars).toContain('secondVar');
});

tap.test('', async () => {
  let templateString =
    '{{{firstVar}}} {{secondVar}} {{thirdVar}} {{fourthVar}} {{fourthVar.someKey}} {{fourthVar.otherKey.nextKey}}';
  let missingVars = await smarthbs.checkVarsSatisfaction(templateString, {
    firstVar: 'hi',
    secondVar: 'hello',
    fourthVar: {
      otherKey: {
        nextKey: 'wow',
      },
    },
  });
  expect(missingVars).toContain('thirdVar');
  expect(missingVars).toContain('fourthVar.someKey');
  expect(missingVars).not.toContain('secondVar');
  expect(missingVars).not.toContain('fourthVar.otherKey.nextKey');
});

tap.start();
